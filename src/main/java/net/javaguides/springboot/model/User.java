package net.javaguides.springboot.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Table(name= "users")
@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name= "fname", nullable = false)
	
	//user fname should not be null or empty
	//user fname should have at least 3 characters
	
	@NotEmpty
	@Size(min=3, message = "user firstname should have at least 3 characters")
	private String firstName;
	
	//user lname should not be null or empty
	//user lname should have at least 3 charcters
	
	@NotEmpty
	@Size(min=3, message = "user lastname should have at least 3 characters")
	private String lastName;
	
	//email should be valid email format
	//email should not be null or empty
	
	@NotEmpty
	@Email
	private String email;
	
	//password should not be null or empty
	//password should have 5 characters
	
	@NotEmpty
	@Size(min = 5,message = "password should have at least 5 characters" )
	private String password;
	
	public User() {
		
	}
	
	public User(String firstName, String lastName, String email, String password) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
